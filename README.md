# task20_firewalld
Сценарии iptables
1) реализовать knocking port - centralRouter может попасть на ssh inetrRouter через knock скрипт, пример в материалах
2) добавить inetRouter2, который виден(маршрутизируется) с хоста
3) запустить nginx на centralServer
4) пробросить 80й порт на inetRouter2 8080
5) дефолт в инет оставить через inetRouter


----------------------------------------------------------------------------
Задание 1

```
Воспользуюсь firewalld при настройке, не привычно, но удобно настраивать разные правила на разных интерфейсах
на inetRouter1 перемещу интерфейс смотрящий на centralRouter в отдельную зону internal и отключим в ней ssh
заметьте vagrant ssh оставляю как есть чтоб нормальный доступ иметь к inetRouter1

Следующие команды делаем на inetRouter1
[root@inetRouter1 vagrant]# systemctl start firewalld - слетают все старые правила, включая маскарадинг, починим

[root@inetRouter1 vagrant]# firewall-cmd --permanent --zone=public --remove-interface=eth1
[root@inetRouter1 vagrant]# firewall-cmd --permanent --zone=internal --add-interface=eth1
[root@inetRouter1 vagrant]# firewall-cmd --permanent --zone=public --add-masquerade && firewall-cmd --reload
[root@inetRouter1 vagrant]# firewall-cmd --zone=internal --permanent --remove-service=ssh && firewall-cmd --reload
----------------------------------------------------------------------------
настроим демон knockd
[root@inetRouter1 vagrant]# yum install libpcap
[root@inetRouter1 vagrant]# rpm -Uvh http://li.nux.ro/download/nux/dextop/el7Server/x86_64/knock-server-0.7-1.el7.nux.x86_64.rpm
Приведем конфиг к нужному виду 

[root@inetRouter1 vagrant]# cat /etc/knockd.conf
[options]
UseSyslog
logfile = /var/log/knockd.log
[OpenSSH]
Sequence = 1111,2222,3333
Seq_timeout = 15
Tcpflags = syn
Command = /bin/firewall-cmd --zone=internal --add-rich-rule="rule family="ipv4" source address="%IP%" service name="ssh" accept"

[CloseSSH]
Sequence = 6666,7777,8888
Seq_timeout = 15
Tcpflags = syn
Command = /bin/firewall-cmd --zone=internal --remove-rich-rule="rule family="ipv4" source address="%IP%" service name="ssh" accept"


Заставим его слушать на нужном порту
[root@inetRouter1 vagrant]# echo 'OPTIONS="-i eth1"' >> /etc/sysconfig/knockd
и запустим

надо не забыть задать пароль пользователю vagrant 
и в /etc/ssh/ssh_config прописать PasswordAuthentication yes
[root@inetRouter1 vagrant]# sshd -T && systemctl restart sshd
----------------------------------------------------------------------------

Пробуем логинться
[vagrant@centralRouter ~]$ for x in 1111 2222 3333; do nmap -Pn --host_timeout 201 --max-retries 0 -p $x 192.168.255.1 && sleep 1; done && ssh vagrant@192.168.255.1

В это время смотрим в лог knockd
[root@inetRouter1 vagrant]# tail -f /var/log/knockd.log
[2019-11-04 15:24] starting up, listening on eth0
[2019-11-04 15:24] waiting for child processes...
[2019-11-04 15:24] shutting down
[2019-11-04 15:24] starting up, listening on eth0
[2019-11-04 15:26] waiting for child processes...
[2019-11-04 15:26] shutting down
[2019-11-04 15:26] starting up, listening on eth1
[2019-11-04 15:27] 192.168.255.2: OpenSSH: Stage 1
[2019-11-04 15:27] 192.168.255.2: OpenSSH: Stage 2
[2019-11-04 15:27] 192.168.255.2: OpenSSH: Stage 3
[2019-11-04 15:27] 192.168.255.2: OpenSSH: OPEN SESAME
[2019-11-04 15:27] OpenSSH: running command: /bin/firewall-cmd --zone=internal --add-rich-rule="rule family="ipv4" source address="192.168.255.2" service name="ssh" accept"


профит, но все еще тяжеловато
все остальное буду делать через iptables
```
Задание 2 
```
[root@centralServer vagrant]# ping 192.168.255.5
PING 192.168.255.5 (192.168.255.5) 56(84) bytes of data.
64 bytes from 192.168.255.5: icmp_seq=1 ttl=63 time=8.87 ms
64 bytes from 192.168.255.5: icmp_seq=2 ttl=63 time=0.620 ms
```
Задание 3
```
[root@centralServer vagrant]# curl -I localhost:80
HTTP/1.1 200 OK
Server: nginx/1.16.1
Date: Mon, 04 Nov 2019 15:55:06 GMT
```
Задание 4 
```
[root@centralRouter vagrant]# ip r del default
[root@centralRouter vagrant]# ip r add default via 192.168.255.5 dev eth2

[root@inetRouter2 vagrant]# iptables -t nat -D POSTROUTING ! -d 192.168.0.0/24 -o eth0 -j MASQUERADE
[root@inetRouter2 vagrant]# iptables -t nat -A PREROUTING -p tcp -m tcp --dport 80 -j DNAT --to-destination 192.168.0.2:80
[root@inetRouter2 vagrant]# iptables -t nat -A POSTROUTING -j MASQUERADE

теперь при заходе на localhost:8080 я вижу дефолтную страничку nginx
```
Задание 5
```
[root@centralRouter vagrant]# ip r del default
[root@centralRouter vagrant]# ip r add default via 192.168.255.1 dev eth1

Сейчас выглядит так
[root@centralRouter vagrant]# ip r
default via 192.168.255.1 dev eth1 proto static metric 100
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 101
192.168.0.0/28 dev eth3 proto kernel scope link src 192.168.0.1 metric 103
192.168.255.0/30 dev eth1 proto kernel scope link src 192.168.255.2 metric 100
192.168.255.4/30 dev eth2 proto kernel scope link src 192.168.255.6 metric 102

Сайт все равно открывается без проблем, даже не надо делать никаких манипуляций ввиде
(Перезагружать тоже пробовал)

iptables -A PREROUTING -i eth2 -t mangle -p tcp --dport 80 -j MARK --set-mark 1
echo 201 http.out >> etc/iproute/rt_tables
ip rule add fwmark 1 table http.out
ip route add default via 192.168.255.5 dev eth1 table http.out

```

